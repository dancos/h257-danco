package com.example.danco.homework7.h257danco.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by danco on 2/21/15.
 */
public class ContactsDataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;


    public ContactsDataSource(Context context) {
        this.dbHelper = DBHelper.getInstance(context);
    }


    public void open() {
        database = dbHelper.getWritableDatabase();
    }


    public void close() {
        dbHelper.close();
    }


    public long addContact(final String name, final String address, final long birthDate) {
        Contact contact = null;
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, name);
        values.put(ContactContract.Columns.ADDRESS, address);
        values.put(ContactContract.Columns.BIRTH_DATE, birthDate);

        long insertId = database.insert(ContactContract.TABLE, null, values);
        return insertId;
    }
}
