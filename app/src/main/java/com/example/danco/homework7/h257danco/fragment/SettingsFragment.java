package com.example.danco.homework7.h257danco.fragment;


import android.os.Bundle;
import android.preference.PreferenceFragment;


/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(com.example.danco.homework7.h257danco.R.xml.settings);
    }
}
