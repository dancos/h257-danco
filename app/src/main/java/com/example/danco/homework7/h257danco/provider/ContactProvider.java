package com.example.danco.homework7.h257danco.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import java.util.Locale;

public class ContactProvider extends ContentProvider implements ContactContract {

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int CONTACT = 1;
    private static final int CONTACT_ID = 2;

    private static final String WHERE_MATCHES_ID = BaseColumns._ID + " = ?";

    static {
        uriMatcher.addURI(ContactContract.AUTHORITY, null, CONTACT);
        uriMatcher.addURI(ContactContract.AUTHORITY, "#", CONTACT_ID);

        // If had a "table" as an added path of Uri, then this becomes:
        //uriMatcher.addURI(StateContract.AUTHORITY, StateContract.TABLE, STATE);
        //uriMatcher.addURI(StateContract.AUTHORITY, StateContract.TABLE + "/#", STATE_ID);
    }

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = DBHelper.getInstance(getContext());
        return true;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;
        switch (match) {
            case CONTACT_ID:
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CONTACT:
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Non Exclusive required for write ahead logging
        int rows = 0;
        db.beginTransactionNonExclusive();
        try {
            rows = db.delete(TABLE, useSelection, useSelectionArgs);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        notifyChange(uri);
        return rows;
    }


    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CONTACT:
                return ContactContract.CONTENT_TYPE;
            case CONTACT_ID:
                return ContactContract.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int match = uriMatcher.match(uri);
        switch (match) {
            case CONTACT:
                break;
            case CONTACT_ID:
                throw new UnsupportedOperationException("Unable to insert by id. uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        long id = -1;
        // if using writeAheadLogging be consistent and use it everywhere.
        db.beginTransactionNonExclusive();
        try {
            // see also insertWithOnConflict CONFLICT_IGNORE, CONFLICT_REPLACE, etc.
            // in this case we're using insertOrThrow to bubble the error up, prod might
            //  prefer to use insert where a simple -1 gets returned.
            id = db.insertOrThrow(ContactContract.TABLE, null, values);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        notifyChange(uri);
        return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] valuesArray) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int match = uriMatcher.match(uri);
        switch (match) {
            case CONTACT:
                break;
            case CONTACT_ID:
                throw new UnsupportedOperationException("Unable to insert by id. uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Example of bulk inserting under one transaction. This should be better
        // performing than doing it individually.
        int rowsInserted = 0;
        db.beginTransactionNonExclusive();
        try {
            for (ContentValues values : valuesArray) {
                long id = db.insert(TABLE, null, values);

                // This code ignores errors, you may not want to do that...
                if (id != -1) {
                    ++rowsInserted;
                }
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data (instead of reading the table after each update)
        notifyChange(uri);
        return rowsInserted;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;
        switch (match) {
            case CONTACT_ID:
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CONTACT:
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Cursor result = db.query(ContactContract.TABLE, projection, useSelection, useSelectionArgs,
                null, null, sortOrder);

        // Register the cursor with the requested URI so the caller will receive
        // future database change notifications. Useful for "loaders: which take advantage
        // of this concept.
        result.setNotificationUri(getContext().getContentResolver(), uri);
        return result;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;
        switch (match) {
            case CONTACT_ID:
                useSelection = WHERE_MATCHES_ID;
                // URI supercedes the query parameters
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CONTACT:
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        int rows = 0;
        db.beginTransactionNonExclusive();
        try {
            rows = db.update(ContactContract.TABLE, values, useSelection, useSelectionArgs);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        notifyChange(uri);
        return rows;
    }


    /**
     * Helper method to notify listeners of the changes to the database. Useful with loaders
     *
     * @param uri the URI for the content that changed.
     */
    private void notifyChange(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null, false);
    }
}
