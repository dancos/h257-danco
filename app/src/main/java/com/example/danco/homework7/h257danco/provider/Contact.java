package com.example.danco.homework7.h257danco.provider;

import java.util.Date;

/**
 * Created by danco on 2/21/15.
 */
public class Contact {

    private String name;
    private String address;
    private Date birthDate;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public Date getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
