package com.example.danco.homework7.h257danco.provider;

import android.content.ContentValues;

import java.util.Calendar;

public class DataUtilities {

    /* package */
    static ContentValues createContactValues() {
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, "Dan Costinett");
        values.put(ContactContract.Columns.ADDRESS, "925 4th Ave S., \nSeattle, WA 98104");
        Calendar date = Calendar.getInstance();
        date.set(1967, Calendar.AUGUST, 6, 0, 0, 0);
        values.put(ContactContract.Columns.BIRTH_DATE, date.getTimeInMillis());
        return values;
    }

    /* package */
    static ContentValues updateContactValues() {
        ContentValues values = new ContentValues();
        values.put(ContactContract.Columns.NAME, "Dylan Costinett");
        values.put(ContactContract.Columns.ADDRESS, "925 4th Ave S., \nSeattle, WA 98777");
        Calendar date = Calendar.getInstance();
        date.set(2001, Calendar.MAY, 20, 0, 0, 0);
        values.put(ContactContract.Columns.BIRTH_DATE, date.getTimeInMillis());
        return values;
    }

    public static ContentValues[] mergeValues(ContentValues[] targetArray, ContentValues source) {
        for (ContentValues target: targetArray) {
            mergeValues(target, source);
        }
        return targetArray;
    }

    public static ContentValues mergeValues(ContentValues target, ContentValues source) {
        target.putAll(source);
        return target;
    }

    public static ContentValues[] createBulkValues() {
        ContentValues[] bulkValues = new ContentValues[2];
        bulkValues[0] = createContactValues();
        bulkValues[1] = updateContactValues();
        return bulkValues;
    }

}
