package com.example.danco.homework7.h257danco.provider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;

/**
 * Created by costd035 on 2/23/15.
 */
public class ContactProviderTest2 extends ProviderTestCase2<ContactProvider> {

    public ContactProviderTest2(Class<ContactProvider> providerClass, String providerAuthority) {
        super(providerClass, providerAuthority);
    }


    public ContactProviderTest2() {
        super(ContactProvider.class, ContactContract.AUTHORITY);
    }


    public void setUp() throws Exception {
        super.setUp();
        cleanupDB();
    }


    private void cleanupDB() {
        DBHelper helper = DBHelper.getInstance(getContext());
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(ContactContract.TABLE, null, null);
    }


    public void testBasicCRUD() throws Exception {
        ContentValues insertData = DataUtilities.createContactValues();
        Uri uri = getContext().getContentResolver().insert(ContactContract.URI, insertData);
        assertNotNull(uri);

        long id = ContentUris.parseId(uri);

        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        CursorUtilities.validateCursor("Insert", cursor, insertData, id);

        // Update our only row with a new statehood date using the "uri" provided on insert
        ContentValues updateData = DataUtilities.updateContactValues();
        int rows = getContext().getContentResolver().update(uri, updateData, null, null);
        assertEquals(1, rows);


        ContentValues mergedData = DataUtilities.mergeValues(insertData, updateData);
        cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        CursorUtilities.validateCursor("Update", cursor, mergedData, id);

        rows = getContext().getContentResolver().delete(uri, null, null);
        assertEquals(1, rows);

        cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        assertNotNull(cursor);
        assertEquals(0, cursor.getCount());
    }

    public void testBulkCRUD() throws Exception {
        ContentValues[] insertData = DataUtilities.createBulkValues();

        int rows = getContext().getContentResolver().bulkInsert(ContactContract.URI, insertData);
        assertEquals(2, rows);

        Cursor cursor = getContext().getContentResolver().query(ContactContract.URI, null, null, null, null);
        CursorUtilities.validateCursor("Bulk Insert", cursor, insertData);

        ContentValues updateData = DataUtilities.updateContactValues();
        rows = getContext().getContentResolver().update(ContactContract.URI, updateData, null, null);
        assertEquals(2, rows);

        ContentValues[] mergedData = DataUtilities.mergeValues(insertData, updateData);
        cursor = getContext().getContentResolver().query(ContactContract.URI, null, null, null, null);
        CursorUtilities.validateCursor("Update", cursor, mergedData);

        rows = getContext().getContentResolver().delete(ContactContract.URI, null, null);
        assertEquals(2, rows);

        cursor = getContext().getContentResolver().query(ContactContract.URI, null, null, null, null);
        assertNotNull(cursor);
        assertEquals(0, cursor.getCount());
    }

    public void testErrors() throws Exception {
        // insert a valid row
        ContentValues insertData = DataUtilities.createContactValues();
        Uri uri = getContext().getContentResolver().insert(ContactContract.URI, insertData);
        assertNotNull(uri);

        // Attempt to insert using returned uri.
        boolean hadException = false;
        try {
            uri = getContext().getContentResolver().insert(uri, insertData);
        }
        catch (UnsupportedOperationException e) {
            hadException = true;
        }
        assertTrue(hadException);

        Uri errorUri = ContactContract.URI.buildUpon().appendPath("error").build();
        hadException = false;
        try {
            uri = getContext().getContentResolver().insert(errorUri, insertData);
        }
        catch (UnsupportedOperationException e) {
            hadException = true;
        }
        assertTrue(hadException);

        hadException = false;
        try {
            Cursor cursor = getContext().getContentResolver().query(errorUri, null, null, null, null);
        }
        catch (UnsupportedOperationException e) {
            hadException = true;
        }
        assertTrue(hadException);

        hadException = false;
        int rows = 0;
        try {
            rows = getContext().getContentResolver().update(errorUri, insertData, null, null);
        }
        catch (UnsupportedOperationException e) {
            hadException = true;
        }
        assertTrue(hadException);

        hadException = false;
        try {
            rows = getContext().getContentResolver().delete(errorUri, null, null);
        }
        catch (UnsupportedOperationException e) {
            hadException = true;
        }
        assertTrue(hadException);
    }
}
