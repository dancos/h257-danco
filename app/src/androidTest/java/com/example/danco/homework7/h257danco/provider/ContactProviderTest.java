package com.example.danco.homework7.h257danco.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.test.AndroidTestCase;

import java.util.Map;
import java.util.Set;

/**
 * Created by costd035 on 2/23/15.
 */
public class ContactProviderTest extends AndroidTestCase {

    public void setUp() throws Exception {
        super.setUp();
        cleanupDB();
    }


    private void cleanupDB() {
        DBHelper helper = DBHelper.getInstance(getContext());
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(ContactContract.TABLE, null, null);
    }


    public void testGetType() throws Exception {
        // Verify the right type is returned using the standard URI
        String type = getContext().getContentResolver().getType(ContactContract.URI);
        assertEquals(ContactContract.CONTENT_TYPE, type);

        // Verify the right type is returned using a URI with an _ID attached.
        type = getContext().getContentResolver().getType(
                ContentUris.withAppendedId(ContactContract.URI, 1));
        assertEquals(ContactContract.CONTENT_ITEM_TYPE, type);
    }


    public void testEmptyUri() throws Exception {
        Uri emptyUri = Uri.EMPTY;

        String type = getContext().getContentResolver().getType(emptyUri);
        assertNull(type);
    }
}
